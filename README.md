<html>
    <head>
        <title>Application</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    </head>
    <body>
        <h1>Application</h1>
        <p id="countItems"></p>
        <script>
            setLike = false;
            function incrementLikes(likesClass) {
                if (setLike === false) {
                    likesClassIncrement = likesClass;
                }
                likesClassIncrement++;
                $(".likes" + likesClass).html(likesClassIncrement);
                setLike = true;
            }
            function addItems() {
                var value = $("#value").val();
                var likes = $("#likes").val();
                var button = "<button onclick='incrementLikes(this.value)' type='button'>Increment</button>";
                $("#addItems").append("<tr><td>" + value + "</td><td>" + likes + "</td><td>" + button + "</td></tr>");
            }

            $.ajax({
                type: "GET",
                dataType: "json",
                url: "data.json",
                async: false,
                cache: false,
                success: function (response)
                {
                    document.write("<table id='addItems'>");
                    for (i = 0; i < response.feed.length; i++) {
                        likes = response.feed[i].likes;
                        likesClass = likes;
                        document.write("<tr><td>" + response.feed[i].value + "</td><td class='likes" + likesClass + "'>" + response.feed[i].likes + "</td><td>" + "<button onclick='incrementLikes(this.value)' value='" + likesClass + "'+ type='button'>Increment</button>" + "</td></tr>");

                    }
                    document.write("</table>");
                    $("#countItems").html("Total Count Items: " + i);
                }
            });
        </script>
        <hr>
        Enter value: <input placeholder="value" id="value" type="text"/>
        Enter likes: <input placeholder="likes" id="likes" type="text"/>
        <input type="button" value="Add New Items" onclick="addItems()">
    </body>
</html>